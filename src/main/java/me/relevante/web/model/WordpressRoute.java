package me.relevante.web.model;

public class WordpressRoute {

    public static final String NETWORK = "/wordpress";
    public static final String API_HUBS = CoreRoute.API_PREFIX + NETWORK + "/hubs";
}

