package me.relevante.web.model.json;

/**
 * @author daniel-ibanez
 */
public class WordpressCategoryData {

    private String name;
    private String slug;

    public WordpressCategoryData(String name, String slug) {
        this.name = name;
        this.slug = slug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
