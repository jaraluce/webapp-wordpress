package me.relevante.web.model.json;

import me.relevante.network.Wordpress;

/**
 * @author daniel-ibanez
 */
public class WordpressAccountData implements NetworkAccountData<Wordpress> {

    private String hubId;

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }
}
