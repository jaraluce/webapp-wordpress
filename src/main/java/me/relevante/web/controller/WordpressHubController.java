package me.relevante.web.controller;

import me.relevante.web.model.CoreSessionAttribute;
import me.relevante.web.model.WordpressRoute;
import me.relevante.web.model.json.WordpressCategoryData;
import me.relevante.web.service.WordpressHubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping(value = WordpressRoute.API_HUBS)
public class WordpressHubController {

    @Autowired private WordpressHubService hubService;

    @RequestMapping(value = "/{hubId}/categories",
                    method = RequestMethod.GET)
    public List<WordpressCategoryData> getHubCategories(@PathVariable String hubId,
                                                        HttpSession session) {

        String relevanteId = (String) session.getAttribute(CoreSessionAttribute.RELEVANTE_ID);
        List<WordpressCategoryData> categories = hubService.getCategories(hubId, relevanteId);
        return categories;
    }

}
