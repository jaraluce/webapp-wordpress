package me.relevante.web.persistence;

import me.relevante.model.WordpressFullUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordpressFullUserRepo extends MongoRepository<WordpressFullUser, String> {
}
