package me.relevante.web.persistence;

import me.relevante.model.WordpressHub;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordpressHubRepo extends MongoRepository<WordpressHub, String> {
    WordpressHub findOneByUrl(String url);
}
