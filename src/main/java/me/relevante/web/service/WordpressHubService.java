package me.relevante.web.service;

import me.relevante.api.NetworkBasicAuthCredentials;
import me.relevante.api.WordpressApi;
import me.relevante.api.WordpressApiImpl;
import me.relevante.model.NetworkEntity;
import me.relevante.model.RelevanteAccount;
import me.relevante.model.WordpressCategory;
import me.relevante.model.WordpressHub;
import me.relevante.network.Wordpress;
import me.relevante.web.model.json.WordpressCategoryData;
import me.relevante.web.persistence.WordpressHubRepo;
import me.relevante.web.persistence.RelevanteAccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WordpressHubService implements NetworkEntity<Wordpress> {

    private RelevanteAccountRepo relevanteAccountRepo;
    private WordpressHubRepo hubRepo;

    @Autowired
    public WordpressHubService(RelevanteAccountRepo relevanteAccountRepo,
                               WordpressHubRepo hubRepo) {
        this.relevanteAccountRepo = relevanteAccountRepo;
        this.hubRepo = hubRepo;
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }

    public List<WordpressCategoryData> getCategories(String hubId,
                                                     String relevanteId) {
        RelevanteAccount relevanteAccount = relevanteAccountRepo.findOne(relevanteId);
        NetworkBasicAuthCredentials<Wordpress> credentials = (NetworkBasicAuthCredentials) relevanteAccount.getCredentials(Wordpress.getInstance());
        WordpressHub hub = hubRepo.findOne(hubId);
        WordpressApi wordpressApi = new WordpressApiImpl(hub.getUrl(), credentials.getUsername(), credentials.getPassword());
        List<WordpressCategory> wpCategories = wordpressApi.getCategories();
        List<WordpressCategoryData> categories = new ArrayList<>();
        wpCategories.forEach(wpCategory -> categories.add(new WordpressCategoryData(wpCategory.getName(), wpCategory.getSlug())));
        return categories;
    }
}
