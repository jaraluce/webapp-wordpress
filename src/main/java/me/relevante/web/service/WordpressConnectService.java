package me.relevante.web.service;

import me.relevante.api.OAuthTokenPair;
import me.relevante.api.WordpressApi;
import me.relevante.api.WordpressApiImpl;
import me.relevante.model.WordpressFullUser;
import me.relevante.model.WordpressHub;
import me.relevante.model.WordpressProfile;
import me.relevante.network.Wordpress;
import me.relevante.web.model.oauth.NetworkOAuthManager;
import me.relevante.web.persistence.RelevanteAccountRepo;
import me.relevante.web.persistence.RelevanteContextRepo;
import me.relevante.web.persistence.WordpressHubRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class WordpressConnectService extends AbstractNetworkConnectService<Wordpress, WordpressProfile, WordpressFullUser>
        implements NetworkConnectService<Wordpress, WordpressFullUser> {

    private WordpressHubRepo hubRepo;

    @Autowired
    public WordpressConnectService(RelevanteAccountRepo relevanteAccountRepo,
                                   RelevanteContextRepo relevanteContextRepo,
                                   CrudRepository<WordpressFullUser, String> fullUserRepo,
                                   WordpressHubRepo hubRepo,
                                   QueueService queueService) {
        super(relevanteAccountRepo, relevanteContextRepo, fullUserRepo, queueService);
        this.hubRepo = hubRepo;
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }

    @Override
    public WordpressFullUser connectNetworkByOAuth(String relevanteId,
                                                   String oAuthRequestToken,
                                                   String oAuthRequestSecret,
                                                   String oAuthRequestVerifier) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected NetworkOAuthManager<Wordpress> createNetworkOAuthManager() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected WordpressProfile createProfileFromOAuthData(OAuthTokenPair<Wordpress> oAuthTokenPair) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected WordpressProfile createProfileFromBasicAuthData(String url, String username, String password) {
        WordpressApi wpApi = new WordpressApiImpl(url, username, password);
        WordpressProfile profile = wpApi.getUserData();
        return profile;
    }

    @Override
    protected WordpressFullUser createFullUserFromProfile(WordpressProfile profile) {
        WordpressFullUser fullUser = new WordpressFullUser(profile);
        WordpressHub hub = hubRepo.findOneByUrl(profile.getUrl());
        if (hub == null) {
            hub = new WordpressHub();
            hub.setUrl(profile.getUrl());
            hub = hubRepo.save(hub);
        }
        fullUser.setHubId(hub.getId());
        fullUser.setUsername(profile.getName());
        return fullUser;
    }

}
