package me.relevante.web.service;

import me.relevante.model.WordpressFullUser;
import me.relevante.network.Wordpress;
import me.relevante.web.model.json.NetworkAccountData;
import me.relevante.web.model.json.WordpressAccountData;
import me.relevante.web.persistence.WordpressFullUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author daniel-ibanez
 */
@Service
public class WordpressAccountDataService extends AbstractNetworkAccountDataService<Wordpress, WordpressFullUser> implements NetworkAccountDataService<Wordpress> {

    @Autowired
    public WordpressAccountDataService(WordpressFullUserRepo fullUserRepo) {
        super(fullUserRepo);
    }

    @Override
    protected NetworkAccountData<Wordpress> mapFullUserToAccountData(WordpressFullUser fullUser) {

        WordpressAccountData wordpressAccountData = new WordpressAccountData();
        wordpressAccountData.setHubId(fullUser.getHubId());

        return wordpressAccountData;
    }

    @Override
    public Wordpress getNetwork() {
        return Wordpress.getInstance();
    }
}
